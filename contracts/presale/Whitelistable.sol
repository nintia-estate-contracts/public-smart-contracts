// contracts/Whitelistable.sol
// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "@openzeppelin/contracts/access/Ownable.sol";

contract Whitelistable is Ownable {
	bool public whitelistEnabled = true;
	mapping(address => bool) private whitelist;

	event Whitelisted(address indexed _address, bool _status);
	event WhitelistEnabled();
	event WhitelistDisabled();

	/**
	 * @dev Checks if the sender is on whitelist or if the whitelist is over
	 */
	modifier onlyWhitelisted() {
		require(
			!whitelistEnabled || isSenderWhitelisted(),
			"You are not in the whitelist"
		);
		_;
	}

	constructor() {}

	/**
	 * @dev Set whitelist _addresses status to true (in whitelist) or false (not in whitelist)
	 */
	function setWhitelistAddresses(address[] calldata _addresses, bool _status)
		external
		onlyOwner
	{
		for (uint256 i = 0; i < _addresses.length; i++) {
			whitelist[_addresses[i]] = _status;
			emit Whitelisted(_addresses[i], _status);
		}
	}

	/**
	 * @dev Enable or disable whitelisting filter
	 */
	function changeWhitelistStatus(bool _enabled) external onlyOwner {
		whitelistEnabled = _enabled;
		if (_enabled) {
			emit WhitelistEnabled();
		} else {
			emit WhitelistDisabled();
		}
	}

	/**
	 * @dev Check if the sender is whitelisted
	 */
	function isSenderWhitelisted() public view returns (bool) {
		return whitelist[msg.sender];
	}

	/**
	 * @dev Check if some _address is whitelisted
	 */
	function isAddressWhitelisted(address _address) public view returns (bool) {
		return whitelist[_address];
	}
}
