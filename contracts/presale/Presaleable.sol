// contracts/Presaleable.sol
// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/utils/math/SafeMath.sol";
import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "./Redeemable.sol";
import "./Whitelistable.sol";
import "../libraries/CustomSafeMath.sol";

/**
 * @dev This Smart Contract contains all the variables
 * that Nintia Estate's presale needs. It has a whitelist
 * system and a redemption system. This means that you will
 * not receive the NFT at the presale, you will receive it
 * days later in the claiming phase. This system was first
 * implemented by Axie Infinity's presale, and we based our
 * presale on theirs. You can see their code here:
 *
 * https://github.com/axieinfinity/public-smart-contracts/blob/master/contracts/presale/AxiePresale.sol
 */
abstract contract Presaleable is Whitelistable, Redeemable {
	using SafeMath for uint256;
	using SafeMath16 for uint16;
	using SafeMath8 for uint8;

	uint256 public PRESALE_START_TIMESTAMP;
	uint256 public PRESALE_END_TIMESTAMP;
	uint16 public MAX_PURCHASES_PER_WALLET;

	mapping(uint8 => bool) public validTypes;
	mapping(uint8 => uint256) public prices;
	mapping(uint8 => uint16) public maxTotalSales;
	mapping(uint8 => uint16) public maxTotalGiveaways;

	mapping(uint8 => uint256) public totalSoldByType;
	mapping(uint8 => uint256) public totalGaveawayByType;

	// Mapping address to type to number of nfts purchased
	mapping(address => mapping(uint8 => uint16)) public buyers;

	event NFTAcquired(address indexed _buyer, uint8 _type);
	event NFTGaveaway(address indexed _buyer, uint8 _type);
	event NFTRedeemed(address indexed _buyer, uint8 _type);

	/**
	 * @dev Checks if the current time is between the presale start and end time
	 */
	modifier onlyDuringPresale() {
		require(
			block.timestamp >= PRESALE_START_TIMESTAMP,
			"Presale has not started"
		);
		require(block.timestamp <= PRESALE_END_TIMESTAMP, "Presale ended");
		_;
	}

	/**
	 * @dev Checks if a given _type of NFTs is valid (meaning it is used on this presale)
	 */
	modifier onlyValidType(uint8 _type) {
		require(validTypes[_type], "Given type is not valid");
		_;
	}

	/**
	 * @dev Transfer all BNB held by the contract to the owner.
	 */
	function claimBNB() external onlyOwner {
		payable(owner()).transfer(address(this).balance);
	}

	/**
	 * @dev Transfer all ERC20 of tokenContract held by contract to the owner.
	 */
	function claimERC20(address _tokenContract) external onlyOwner {
		require(_tokenContract != address(0), "Invalid address");
		IERC20 token = IERC20(_tokenContract);
		uint256 balance = token.balanceOf(address(this));
		token.transfer(owner(), balance);
	}

	/**
	 * @dev Set the prices of the NFTs. Used to change the BNB prices minutes before the presale.
	 */
	function setTypePrice(uint8 _type, uint256 _newPrice)
		external
		onlyOwner
		onlyValidType(_type)
	{
		require(_newPrice > 0, "New price must be higher than 0");
		prices[_type] = _newPrice;
	}

	/**
	 * @dev Get the number of NFTs purchased by an address _buyer and by _type
	 */
	function getTotalPurchasedByAddressAndType(address _buyer, uint8 _type)
		public
		view
		returns (uint16)
	{
		return buyers[_buyer][_type];
	}

	/**
	 * @dev Get the number of NFTs purchased by an address _buyer
	 */
	function getTotalPurchasedByAddress(address _buyer)
		public
		view
		virtual
		returns (uint16)
	{
		return 0;
	}

	/**
	 * @dev Buys a given _type of NFT.
	 */
	function _buy(uint8 _type) internal {
		require(msg.value >= prices[_type], "Not enough BNB");
		require(
			getTotalPurchasedByAddress(msg.sender).add(1) <=
				MAX_PURCHASES_PER_WALLET,
			"You have reached the purchasing limit of this NFT"
		);
		require(
			totalSoldByType[_type].add(1) <= maxTotalSales[_type],
			"This NFT type is sold out"
		);

		// Refund back the remaining funds to the buyer
		uint256 change = msg.value.sub(prices[_type]);
		payable(msg.sender).transfer(change);

		// Update purchase counters
		buyers[msg.sender][_type] = buyers[msg.sender][_type].add(1);
		totalSoldByType[_type] = totalSoldByType[_type].add(1);

		emit NFTAcquired(msg.sender, _type);
	}

	/**
	 * @dev Giveaway to a list of _addresses a given _type of NFT
	 */
	function _giveaway(address[] calldata _addresses, uint8 _type) internal {
		require(
			totalGaveawayByType[_type].add(_addresses.length) <=
				maxTotalGiveaways[_type],
			"The number of giveaways of this NFT has reached its limit"
		);

		// Give NFTs to given addresses
		for (uint16 i = 0; i < _addresses.length; i++) {
			buyers[_addresses[i]][_type] = buyers[_addresses[i]][_type].add(1);
			emit NFTGaveaway(_addresses[i], _type);
		}
		totalGaveawayByType[_type] = totalGaveawayByType[_type].add(
			_addresses.length
		);
	}

	/**
	 * @dev Redeems the previously purchased NFTs for a given _buyer and _type
	 */
	function _redeem(address _buyer, uint8 _type) internal {
		require(
			buyers[_buyer][_type] >= 1,
			"You dont have NFTs of this type to redeem"
		);

		buyers[_buyer][_type] = buyers[_buyer][_type].sub(1);
		emit NFTRedeemed(_buyer, _type);
	}
}
