// contracts/Redeemable.sol
// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "@openzeppelin/contracts/access/Ownable.sol";

contract Redeemable is Ownable {
	address public redemptionAddress;

	/**
	 * @dev Checks if the address calling the redemption method is the redemption address
	 */

	modifier onlyRedemptionAddress() {
		require(
			msg.sender == redemptionAddress,
			"You are not the redemption address"
		);
		_;
	}

	constructor() {}

	/**
	 * @dev Sets the redemption address to redeem NFTs after the presale
	 */
	function setRedemptionAddress(address _redemptionAddress)
		external
		onlyOwner
	{
		redemptionAddress = _redemptionAddress;
	}
}
