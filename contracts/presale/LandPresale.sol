// contracts/LandPresale.sol
// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "@openzeppelin/contracts/security/Pausable.sol";
import "@openzeppelin/contracts/utils/math/SafeMath.sol";
import "../libraries/CustomSafeMath.sol";
import "./Whitelistable.sol";
import "./Presaleable.sol";

contract LandPresale is Presaleable, Pausable {
	using SafeMath for uint256;
	using SafeMath16 for uint16;
	using SafeMath8 for uint8;

	uint8 public constant TYPE_RURAL = 1;
	uint8 public constant TYPE_NEUTRAL = 3;
	uint8 public constant TYPE_BEACH = 5;
	uint8 public constant TYPE_URBAN = 9;

	uint256 public constant PRICE_RURAL = 150000000000000000; // 0.15 BNB
	uint256 public constant PRICE_NEUTRAL = 183330000000000000; // 0.18333 BNB
	uint256 public constant PRICE_BEACH = 241660000000000000; // 0.24166 BNB
	uint256 public constant PRICE_URBAN = 291660000000000000; // 0.29166 BNB

	uint16 public constant MAX_TOTAL_SALES_RURAL = 1000;
	uint16 public constant MAX_TOTAL_SALES_NEUTRAL = 830;
	uint16 public constant MAX_TOTAL_SALES_BEACH = 610;
	uint16 public constant MAX_TOTAL_SALES_URBAN = 500;

	uint8 public constant MAX_TOTAL_GIVEAWAY_RURAL = 0;
	uint8 public constant MAX_TOTAL_GIVEAWAY_NEUTRAL = 0;
	uint8 public constant MAX_TOTAL_GIVEAWAY_BEACH = 5;
	uint8 public constant MAX_TOTAL_GIVEAWAY_URBAN = 5;

	constructor() {
		// Setting up timestamp and max purchases
		PRESALE_START_TIMESTAMP = 1637618400;
		PRESALE_END_TIMESTAMP = 1638223200;
		MAX_PURCHASES_PER_WALLET = 3;

		// Setting valid types
		validTypes[TYPE_RURAL] = true;
		validTypes[TYPE_NEUTRAL] = true;
		validTypes[TYPE_BEACH] = true;
		validTypes[TYPE_URBAN] = true;

		// Setting initial prices
		prices[TYPE_RURAL] = PRICE_RURAL;
		prices[TYPE_NEUTRAL] = PRICE_NEUTRAL;
		prices[TYPE_BEACH] = PRICE_BEACH;
		prices[TYPE_URBAN] = PRICE_URBAN;

		// Setting max total sales
		maxTotalSales[TYPE_RURAL] = MAX_TOTAL_SALES_RURAL;
		maxTotalSales[TYPE_NEUTRAL] = MAX_TOTAL_SALES_NEUTRAL;
		maxTotalSales[TYPE_BEACH] = MAX_TOTAL_SALES_BEACH;
		maxTotalSales[TYPE_URBAN] = MAX_TOTAL_SALES_URBAN;

		// Setting max total giveaways
		maxTotalGiveaways[TYPE_RURAL] = MAX_TOTAL_GIVEAWAY_RURAL;
		maxTotalGiveaways[TYPE_NEUTRAL] = MAX_TOTAL_GIVEAWAY_NEUTRAL;
		maxTotalGiveaways[TYPE_BEACH] = MAX_TOTAL_GIVEAWAY_BEACH;
		maxTotalGiveaways[TYPE_URBAN] = MAX_TOTAL_GIVEAWAY_URBAN;
	}

	/**
	 * @dev Buys lands of given _type
	 */
	function buyLands(uint8 _type)
		external
		payable
		onlyWhitelisted
		onlyDuringPresale
		whenNotPaused
		onlyValidType(_type)
	{
		_buy(_type);
	}

	/**
	 * @dev Giveaway lands of given _type to given list of _addresses
	 */
	function giveawayLand(address[] calldata _addresses, uint8 _type)
		external
		onlyOwner
		onlyDuringPresale
		whenNotPaused
		onlyValidType(_type)
	{
		_giveaway(_addresses, _type);
	}

	/**
	 * @dev Redeems lands of given _type of the given _buyer
	 */
	function redeemLands(address _buyer, uint8 _type)
		external
		onlyRedemptionAddress
		whenNotPaused
		onlyValidType(_type)
	{
		_redeem(_buyer, _type);
	}

	/**
	 * @dev Get the number of NFTs purchased by an address _buyer
	 */
	function getTotalPurchasedByAddress(address _buyer)
		public
		view
		override
		returns (uint16)
	{
		uint16 totalPurchased = 0;

		totalPurchased = totalPurchased
			.add(getTotalPurchasedByAddressAndType(_buyer, TYPE_RURAL))
			.add(getTotalPurchasedByAddressAndType(_buyer, TYPE_NEUTRAL))
			.add(getTotalPurchasedByAddressAndType(_buyer, TYPE_BEACH))
			.add(getTotalPurchasedByAddressAndType(_buyer, TYPE_URBAN));

		return totalPurchased;
	}

	/**
	 * @dev Get the prices in a bulk request
	 */
	function getPricesList() public view returns (uint256[] memory) {
		uint256[] memory pricesList = new uint256[](4);

		pricesList[0] = prices[TYPE_RURAL];
		pricesList[1] = prices[TYPE_NEUTRAL];
		pricesList[2] = prices[TYPE_BEACH];
		pricesList[3] = prices[TYPE_URBAN];

		return pricesList;
	}

	/**
	 * @dev Get the maxTotalSales in a bulk request
	 */
	function getMaxSalesList() public view returns (uint256[] memory) {
		uint256[] memory maxSalesList = new uint256[](4);

		maxSalesList[0] = maxTotalSales[TYPE_RURAL];
		maxSalesList[1] = maxTotalSales[TYPE_NEUTRAL];
		maxSalesList[2] = maxTotalSales[TYPE_BEACH];
		maxSalesList[3] = maxTotalSales[TYPE_URBAN];

		return maxSalesList;
	}

	/**
	 * @dev Get the totalSold in a bulk request
	 */
	function getTotalSoldList() public view returns (uint256[] memory) {
		uint256[] memory totalSoldList = new uint256[](4);

		totalSoldList[0] = totalSoldByType[TYPE_RURAL];
		totalSoldList[1] = totalSoldByType[TYPE_NEUTRAL];
		totalSoldList[2] = totalSoldByType[TYPE_BEACH];
		totalSoldList[3] = totalSoldByType[TYPE_URBAN];

		return totalSoldList;
	}

	/**
	 * @dev Get the inventory in a bulk request
	 */
	function getInventoryList() public view returns (uint16[] memory) {
		uint16[] memory inventoryList = new uint16[](4);

		inventoryList[0] = buyers[msg.sender][TYPE_RURAL];
		inventoryList[1] = buyers[msg.sender][TYPE_NEUTRAL];
		inventoryList[2] = buyers[msg.sender][TYPE_BEACH];
		inventoryList[3] = buyers[msg.sender][TYPE_URBAN];

		return inventoryList;
	}
}
