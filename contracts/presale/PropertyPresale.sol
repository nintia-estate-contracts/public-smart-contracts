// contracts/PropertyPresale.sol
// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "@openzeppelin/contracts/security/Pausable.sol";
import "@openzeppelin/contracts/utils/math/SafeMath.sol";
import "../libraries/CustomSafeMath.sol";
import "./Whitelistable.sol";
import "./Presaleable.sol";

contract PropertyPresale is Presaleable, Pausable {
	using SafeMath for uint256;
	using SafeMath16 for uint16;
	using SafeMath8 for uint8;

	uint8 public constant TYPE_MANSION = 2;
	uint8 public constant TYPE_BUILDING = 4;
	uint8 public constant TYPE_SKYSCRAPER = 8;

	uint256 public constant PRICE_MANSION = 83330000000000000; // 0.08333 BNB
	uint256 public constant PRICE_BUILDING = 166660000000000000; // 0.16666 BNB
	uint256 public constant PRICE_SKYSCRAPER = 333330000000000000; // 0.33333 BNB

	uint16 public constant MAX_TOTAL_SALES_MANSION = 800;
	uint16 public constant MAX_TOTAL_SALES_BUILDING = 400;
	uint16 public constant MAX_TOTAL_SALES_SKYSCRAPER = 200;

	uint8 public constant MAX_TOTAL_GIVEAWAY_MANSION = 20;
	uint8 public constant MAX_TOTAL_GIVEAWAY_BUILDING = 10;
	uint8 public constant MAX_TOTAL_GIVEAWAY_SKYSCRAPER = 10;

	constructor() {
		// Setting up timestamp and max purchases
		PRESALE_START_TIMESTAMP = 1637618400;
		PRESALE_END_TIMESTAMP = 1638223200;
		MAX_PURCHASES_PER_WALLET = 3;

		// Setting valid types
		validTypes[TYPE_MANSION] = true;
		validTypes[TYPE_BUILDING] = true;
		validTypes[TYPE_SKYSCRAPER] = true;

		// Setting initial prices
		prices[TYPE_MANSION] = PRICE_MANSION;
		prices[TYPE_BUILDING] = PRICE_BUILDING;
		prices[TYPE_SKYSCRAPER] = PRICE_SKYSCRAPER;

		// Setting max total sales
		maxTotalSales[TYPE_MANSION] = MAX_TOTAL_SALES_MANSION;
		maxTotalSales[TYPE_BUILDING] = MAX_TOTAL_SALES_BUILDING;
		maxTotalSales[TYPE_SKYSCRAPER] = MAX_TOTAL_SALES_SKYSCRAPER;

		// Setting max total giveaways
		maxTotalGiveaways[TYPE_MANSION] = MAX_TOTAL_GIVEAWAY_MANSION;
		maxTotalGiveaways[TYPE_BUILDING] = MAX_TOTAL_GIVEAWAY_BUILDING;
		maxTotalGiveaways[TYPE_SKYSCRAPER] = MAX_TOTAL_GIVEAWAY_SKYSCRAPER;
	}

	/**
	 * @dev Buys properties of given _type
	 */
	function buyProperties(uint8 _type)
		external
		payable
		onlyWhitelisted
		onlyDuringPresale
		whenNotPaused
		onlyValidType(_type)
	{
		_buy(_type);
	}

	/**
	 * @dev Giveaway properties of given _type to given list of _addresses
	 */
	function giveawayProperty(address[] calldata _addresses, uint8 _type)
		external
		onlyOwner
		onlyDuringPresale
		whenNotPaused
		onlyValidType(_type)
	{
		_giveaway(_addresses, _type);
	}

	/**
	 * @dev Redeems properties of given _type of the given _buyer
	 */
	function redeemProperties(address _buyer, uint8 _type)
		external
		onlyRedemptionAddress
		whenNotPaused
		onlyValidType(_type)
	{
		_redeem(_buyer, _type);
	}

	/**
	 * @dev Get the number of NFTs purchased by an address _buyer
	 */
	function getTotalPurchasedByAddress(address _buyer)
		public
		view
		override
		returns (uint16)
	{
		uint16 totalPurchased = 0;

		totalPurchased = totalPurchased
			.add(getTotalPurchasedByAddressAndType(_buyer, TYPE_MANSION))
			.add(getTotalPurchasedByAddressAndType(_buyer, TYPE_BUILDING))
			.add(getTotalPurchasedByAddressAndType(_buyer, TYPE_SKYSCRAPER));

		return totalPurchased;
	}

	/**
	 * @dev Get the prices in a bulk request
	 */
	function getPricesList() public view returns (uint256[] memory) {
		uint256[] memory pricesList = new uint256[](3);

		pricesList[0] = prices[TYPE_MANSION];
		pricesList[1] = prices[TYPE_BUILDING];
		pricesList[2] = prices[TYPE_SKYSCRAPER];

		return pricesList;
	}

	/**
	 * @dev Get the maxTotalSales in a bulk request
	 */
	function getMaxSalesList() public view returns (uint256[] memory) {
		uint256[] memory maxSalesList = new uint256[](3);

		maxSalesList[0] = maxTotalSales[TYPE_MANSION];
		maxSalesList[1] = maxTotalSales[TYPE_BUILDING];
		maxSalesList[2] = maxTotalSales[TYPE_SKYSCRAPER];

		return maxSalesList;
	}

	/**
	 * @dev Get the totalSold in a bulk request
	 */
	function getTotalSoldList() public view returns (uint256[] memory) {
		uint256[] memory totalSoldList = new uint256[](3);

		totalSoldList[0] = totalSoldByType[TYPE_MANSION];
		totalSoldList[1] = totalSoldByType[TYPE_BUILDING];
		totalSoldList[2] = totalSoldByType[TYPE_SKYSCRAPER];

		return totalSoldList;
	}

	/**
	 * @dev Get the inventory in a bulk request
	 */
	function getInventoryList() public view returns (uint16[] memory) {
		uint16[] memory inventoryList = new uint16[](3);

		inventoryList[0] = buyers[msg.sender][TYPE_MANSION];
		inventoryList[1] = buyers[msg.sender][TYPE_BUILDING];
		inventoryList[2] = buyers[msg.sender][TYPE_SKYSCRAPER];

		return inventoryList;
	}
}
